import { Router } from 'express';
import { getProfileInfo, changeProfilePassword, deleteProfile } from '../controllers/users-controller.js';
import checkAuth from '../middlewares/auth.js';

const router = Router();

router.use(checkAuth);

router.get('/', getProfileInfo);

router.patch('/', changeProfilePassword);

router.delete('/', deleteProfile);

export default router;