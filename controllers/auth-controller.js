import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';

import HttpError from '../models/http-error.js';
import User from '../models/user.js';

export const createProfile = async (req, res, next) => {
    const { username, password } = req.body;

    let existingUser;

    try {
        existingUser = await User.findOne({ username });
    } catch (err) {
        return next(new HttpError('Sign up failed', 500));
    }

    if (existingUser) {
        return next(new HttpError('User exists already, please login instead.', 422));
    }

    let hashedPassword;
    try {
        hashedPassword = await bcrypt.hash(password, 10);
    } catch (err) {
        return next(new HttpError('User was not created, try again later', 500));
    }

    const createdUser = new User({
        username,
        password: hashedPassword,
        createdDate: new Date().toISOString()
    });

    try {
        await createdUser.save();
    } catch (err) {
        return next(new HttpError('Sign up failed', 500));
    }

    let token;
    try {
        token = jwt.sign({ userId: createdUser.id, username: createdUser.username }, process.env.JWT_KEY);
    } catch (err) {
        return next(new HttpError('Sign up failed', 500));
    }

    res.status(200)
        .json({
            message: 'Success'
        });
}

export const login = async (req, res, next) => {
    const { username, password } = req.body;

    if (!username) {
        return next(new HttpError('Username is not provided', 400));
    }

    if (!password) {
        return next(new HttpError('Password is not provided', 400));
    }

    let existingUser;
    try {
        existingUser = await User.findOne({ username });
    } catch (err) {
        return next(new HttpError('Login failed', 500));
    }

    if (!existingUser) {
        return next(new HttpError('Invalid creds', 400));
    }

    let isValidPassword = false;
    try {
        isValidPassword = await bcrypt.compare(password, existingUser.password);
    } catch (err) {
        return next(new HttpError('Could not log you in, please try later', 500));
    }

    if (!isValidPassword) {
        return next(new HttpError('Invalid creds', 400));
    }

    let token;
    try {
        token = jwt.sign({ userId: existingUser.id, username: existingUser.username }, process.env.JWT_KEY);
    } catch (err) {
        return next(new HttpError('Login failed', 500));
    }

    res.json({
        message: 'Success',
        jwt_token: token
    });
}