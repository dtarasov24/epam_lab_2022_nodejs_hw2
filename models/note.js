import mongoose from "mongoose";
import uniqueValidator from 'mongoose-unique-validator';

const Schema = mongoose.Schema;

const noteSchema = new Schema({
    completed: { type: Boolean, required: true},
    text: { type: String, required: true },
    createdDate: { type: String, required: true },
    userId: { type: mongoose.Types.ObjectId, required: true, ref: 'User' }
});

noteSchema.plugin(uniqueValidator);

export default mongoose.model('Note', noteSchema);