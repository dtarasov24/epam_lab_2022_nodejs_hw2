import mongoose from "mongoose";
import uniqueValidator from 'mongoose-unique-validator';

const Schema = mongoose.Schema;

const userSchema = new Schema({
    username: { type: String, required: true },
    password: { type: String, required: true },
    createdDate: { type: String, required: true }
});

userSchema.plugin(uniqueValidator);

export default mongoose.model('User', userSchema);