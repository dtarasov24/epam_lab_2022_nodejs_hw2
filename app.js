import express from 'express';
import bodyParser from 'body-parser';

import mongoose from 'mongoose';
import morgan from 'morgan';
import 'dotenv/config';

import authRoutes from './routes/auth-routes.js';
import usersRoutes from './routes/users-routes.js';
import notesRoutes from './routes/notes-routes.js';

const app = express();

app.use(bodyParser.json());
app.use(morgan('tiny'));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');

    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE');
    
    next();
});

app.use('/api/auth', authRoutes);
app.use('/api/users/me', usersRoutes);
app.use('/api/notes', notesRoutes);

app.use((error, req, res, next) => {
    res.status(error.code || 500);
    res.json({ message: error.message || 'An unknown error occured.' });
});

mongoose.connect(
    `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@cluster0.nhtjl.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`
).then(() => {
    app.listen(process.env.PORT || 8080);
}).catch(err => {
    console.log(err);
});
